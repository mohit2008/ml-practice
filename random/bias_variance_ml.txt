The prediction error for any machine learning algorithm can be broken down into
three parts:
 Bias Error
 Variance Error
 Irreducible Error

The irreducible error cannot be reduced regardless of what algorithm is used. It is the error
introduced from the chosen framing of the problem and may be caused by factors like unknown
variables that influence the mapping of the input variables to the output variable.

Bias are the simplifying assumptions made by a model to make the target function easier to
learn. Generally parametric algorithms have a high bias making them fast to learn and easier
to understand but generally less flexible. In turn they have lower predictive performance

Variance is the amount that the estimate of the target function will change if different training
data was used. The target function is estimated from the training data by a machine learning
algorithm, so we should expect the algorithm to have some variance. Ideally, it should not
change too much from one training dataset to the next, meaning that the algorithm is good
at picking out the hidden underlying mapping between the inputs and the output variables.
Machine learning algorithms that have a high variance are strongly influenced by the specifics
of the training data.

 Parametric or linear machine learning algorithms often have a high bias but a low variance.
 Nonparametric or nonlinear machine learning algorithms often have a low bias but a high
variance.

There is no escaping the relationship between bias and variance in machine learning.
 Increasing the bias will decrease the variance.
 Increasing the variance will decrease the bias.
