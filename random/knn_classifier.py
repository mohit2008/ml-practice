import pandas as pd
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score

data = pd.read_csv("train.csv", nrows=1200)
train = pd.DataFrame(data[:1000])

test = pd.DataFrame(data[1000:])


train_x= np.array(train.iloc[:, 1:])
train_y= np.array(train.iloc[:, 0])
test_y = np.array(test.iloc[:, 0])
test_x=np.array(test.iloc[:, 1:])
print("--------------------------")

neigh = KNeighborsClassifier(n_neighbors=3, n_jobs=-1)
neigh.fit(train_x, train_y)

predic = neigh.predict(test_x)

acc = accuracy_score(test_y, predic)
print("Accuracy achieved is " + str(acc * 100) + "%")

print(confusion_matrix(test_y, predic))
