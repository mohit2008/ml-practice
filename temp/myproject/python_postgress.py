import psycopg2
import pprint
import sys
def main():
    # Define our connection string
    conn_string = "host='localhost' dbname='postgres' user='postgres' password='infy@123'"

    # print the connection string we will use to connect
    print("Connecting to database\n	->%s" % (conn_string))

    # get a connection, if a connect cannot be made an exception will be raised here
    conn = psycopg2.connect(conn_string)

    # conn.cursor will return a cursor object, you can use this cursor to perform queries
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM company")
    print("Connected!\n")

    print("The number of parts: ", cursor.rowcount)
    row = cursor.fetchone()

    while row is not None:
        print(row)
        row = cursor.fetchone()

    cursor.close()

if __name__ == "__main__":
    main()