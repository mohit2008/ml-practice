import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import ensemble
import seaborn as sns
from scipy import stats
from sklearn import preprocessing
from sklearn.metrics import mean_squared_error, r2_score, accuracy_score
from sklearn import linear_model
from scipy.special import inv_boxcox
from sklearn.model_selection import train_test_split
from math import sqrt
from sklearn.model_selection import cross_val_score
np.random.seed(23567)

data= pd.read_csv("train_HL442_1.csv", header=0)
data_test= pd.read_csv("test_HL442_1.csv", header=0)





x=(data.iloc[:,0].reshape(-1,1))

# y,lam=stats.boxcox(data.iloc[:,1])
y=(data.iloc[:,1])
plt.scatter(x, data.iloc[:,1])

plt.savefig("Latitude_regression.png")  # save the image
plt.close()  # close the canvas



def generate_model(features, labels, test_feature):
    params = {'n_estimators': 200, 'max_depth': 3, 'min_samples_split': 2,
              'learning_rate': 0.01, 'loss': 'ls'}
    clf = ensemble.GradientBoostingRegressor(**params)
    clf.fit(features, labels)
    prediction=clf.predict(test_feature)
    # lm = linear_model.LinearRegression(normalize=True)
    # lm.fit(features, labels)  # fit the model
    # prediction = lm.predict(test_feature)  # get predictions
    return prediction


def generate_residual_plot(label, prediction):
    plt.scatter(prediction, np.subtract(label, prediction))  # scatter plot
    plt.xlabel("Fitted Value")
    plt.ylabel("Residuals")
    plt.tight_layout()
    plt.hlines(y=0, xmin=min(prediction), xmax=max(prediction), colors='orange', linewidth=3)  # plot ref line

X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.33, random_state=42)
pred= (generate_model(X_train, y_train, X_test))



print("Mean square error for regression model to predict latitude is " + str(
    sqrt(mean_squared_error((y_test), pred))))  # print out mse
print("R squared value for regression model to predict latitude is " + str(
    r2_score((y_test), pred)))  # print out r2


generate_residual_plot(np.exp(y_test), pred)
plt.savefig("Latitude_regression.png")  # save the image
plt.close()  # close the canvas


################
data_final = data_test.copy()
print(data_test)
unseen_data=((data_test.iloc[:,0].reshape(-1,1)))

pred= (generate_model(X_train, y_train, unseen_data))


data_final['R']=pred

print(data_final)
data_final.to_csv("mohit_submission.csv", sep=',',index=False,float_format='%.6f')
